﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Security;
using System.IO;
using System.Web;
using System.Threading;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using static APIdotNet35.Dependencies;
using APIdotNet35.Responses;
using APIdotNet35.Endpoints;
using APIdotNet35.Helpers;
using APIdotNet35;

namespace APIdotNet35
{
    public class Client
    {
        // API rewritten, more simpler than the VRCCore-Standalone API.
        // although not as functional...
        // Universal Class is free to use!

        public Auth Auth { get; set; }
        public Avatars Avatars { get; set; }
        public Config Config { get; set; }
        public Favorites Favorites { get; set; }
        public Friends Friends { get; set; }
        public Moderations Moderations { get; set; }
        public Notifications Notifications { get; set; }
        public Users Users { get; set; }
        public Worlds Worlds { get; set; }

        public Client(string username, string password)
        {
            Auth = new Auth(username, password);
            Avatars = new Avatars();
            Config = new Config();
            Favorites = new Favorites();
            Friends = new Friends();
            Moderations = new Moderations();
            Notifications = new Notifications();
            Users = new Users();
            Worlds = new Worlds();
        }
    }
}
