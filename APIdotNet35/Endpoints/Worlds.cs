﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Security;
using System.IO;
using System.Web;
using System.Threading;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using static APIdotNet35.Dependencies;
using APIdotNet35.Responses;
using APIdotNet35.Endpoints;
using APIdotNet35.Helpers;
using APIdotNet35;

namespace APIdotNet35.Endpoints
{
    public class Worlds
    {
        public WorldRES Get(string id)
        {
            WorldRES worldRES = Universal.uGET<WorldRES>(baseUrl + "worlds/" + id + uAPIKey);
            return worldRES;
        }

        public FileRES GetVRCWData(string id)
        {
            WorldRES worldRES = Universal.uGET<WorldRES>(baseUrl + "worlds/" + id + uAPIKey);

            FileRES fileRES = Universal.uGET<FileRES>(worldRES.assetUrl.Remove(78));
            return fileRES;
        }

        public FileRES GetImageData(string id)
        {
            WorldRES worldRES = Universal.uGET<WorldRES>(baseUrl + "worlds/" + id + uAPIKey);

            FileRES fileRES = Universal.uGET<FileRES>(worldRES.imageUrl.Remove(78));
            return fileRES;
        }

        public List<WorldsRES> FetchList(string Parameters)
        {
            List<WorldsRES> worldsRES = Universal.uGET<List<WorldsRES>>(baseUrl + "worlds/" + uAPIKey + Parameters);
            return worldsRES;
        }

        public List<WorldsRES> FetchList(int? Offset = null, int? N = null, WorldGroups? WorldGroup = null, bool? Featured = null,
            SortOptions? SortOptions = null, UserOptions? UserOptions = null, string UserID = null, string Search = null, 
            string Tags = null, string ExcludeTags = null, ReleaseStatus? ReleaseStatus = null, string User = null)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append(string.Format("&n={0}", N));
            stringBuilder.Append(string.Format("&offset={0}", Offset));

            if (Featured.HasValue)
            {
                stringBuilder.Append(string.Format("&featured={0}", Featured.Value.ToString().ToLowerInvariant()));

                if (Featured.Value && SortOptions.HasValue == false)
                    stringBuilder.Append("&sort=order");
            }
            if (SortOptions.HasValue)
            {
                stringBuilder.Append(string.Format("&sort={0}", SortOptions.Value.ToString().ToLowerInvariant()));

                if (SortOptions.Value == Responses.SortOptions.Popularity && Featured.HasValue == false)
                    stringBuilder.Append("&featured=false");
            }
            if (UserOptions.HasValue)
                stringBuilder.Append(string.Format("&user={0}", UserOptions.Value.ToString().ToLowerInvariant()));
            if (ReleaseStatus.HasValue)
                stringBuilder.Append(string.Format("&releaseStatus={0}", ReleaseStatus.Value.ToString().ToLowerInvariant()));
            if (!string.IsNullOrEmpty(UserID))
                stringBuilder.Append(string.Format("&userId={0}", UserID));
            if (!string.IsNullOrEmpty(User))
                stringBuilder.Append(string.Format("&user={0}", User));
            if (!string.IsNullOrEmpty(Search))
                stringBuilder.Append(string.Format("&search={0}", Search));
            if (!string.IsNullOrEmpty(Tags))
                stringBuilder.Append(string.Format("&tag={0}", Tags));
            if (!string.IsNullOrEmpty(ExcludeTags))
                stringBuilder.Append(string.Format("&notag={0}", ExcludeTags));

            string worldUrl = "worlds/";

            switch (WorldGroup)
            {
                case WorldGroups.Active:
                    worldUrl = "worlds/active";
                    break;
                case WorldGroups.Favorite:
                    worldUrl = "worlds/favorites";
                    break;
                case WorldGroups.Recent:
                    worldUrl = "worlds/recent";
                    break;
                case WorldGroups.Any:
                    worldUrl = "worlds/";
                    break;
            }

            List<WorldsRES> worldsRES = Universal.uGET<List<WorldsRES>>(baseUrl + worldUrl + uAPIKey + stringBuilder.ToString());
            return worldsRES;
        }
    }
}
