﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Security;
using System.IO;
using System.Web;
using System.Threading;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using static APIdotNet35.Dependencies;
using APIdotNet35.Responses;
using APIdotNet35.Endpoints;
using APIdotNet35.Helpers;
using APIdotNet35;

namespace APIdotNet35.Endpoints
{
    public class Users
    {
        public UserRES Get(string id)
        {
            UserRES userRES = Universal.uGET<UserRES>(baseUrl + "users/" + id + uAPIKey);
            return userRES;
        }

        public UserRES GetByName(string Name)
        {
            UserRES userRES = Universal.uGET<UserRES>(baseUrl + "users/" + Name + "/name" + uAPIKey);
            return userRES;
        }

        public List<UserRES> FetchList(string Parameters, bool? ActiveUsers = null)
        {
            string endpoint = "users";

            if (ActiveUsers.HasValue && ActiveUsers == true)
                endpoint = "users/active";

            List<UserRES> userRES = Universal.uGET<List<UserRES>>(baseUrl + endpoint + uAPIKey + Parameters);
            return userRES;
        }

        public List<UserRES> FetchList(string Search = null, DeveloperType? DeveloperType = null, int? N = null, int? Offset = null, bool? ActiveUsers = null)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append(string.Format("&n={0}", N));
            stringBuilder.Append(string.Format("&offset={0}", Offset));

            if (DeveloperType.HasValue)
                stringBuilder.Append(string.Format("&developerType={0}", DeveloperType.Value.ToString().ToLowerInvariant()));
            if (!string.IsNullOrEmpty(Search))
                stringBuilder.Append(string.Format("&search={0}", Search));

            string endpoint = "users";

            if (ActiveUsers.HasValue && ActiveUsers == true)
                endpoint = "users/active";

            List<UserRES> userRES = Universal.uGET<List<UserRES>>(baseUrl + endpoint + uAPIKey + stringBuilder.ToString());
            return userRES;
        }

        public NotificationRES SendFriendrequest(string UserID, string SenderName)
        {
            JObject json = new JObject();
            json["type"] = "friendrequest";
            json["message"] = string.Format("{0} wants to be your friend", SenderName);

            NotificationRES notificationRES = Universal.uPOST<NotificationRES>(baseUrl + "user/" + UserID + "/notification" + uAPIKey, json, true);
            return notificationRES;
        }

        public BasicRES AcceptFriendrequest(string UserID)
        {
            BasicRES basicRES = Universal.uPUT<BasicRES>(baseUrl + "auth/user/notifications" + UserID + "/accept" + uAPIKey, "", true);
            return basicRES;
        }

        public BasicRES DeleteFriendrequest(string UserID)
        {
            BasicRES basicRES = Universal.uDELETE<BasicRES>(baseUrl + "auth/user/notifications" + UserID + "/accept" + uAPIKey);
            return basicRES;
        }

        public NotificationRES HideFriendrequestMk1(string UserID)
        {
            NotificationRES notificationRES = Universal.uPUT<NotificationRES>(baseUrl + "auth/user/notifications" + UserID + "/hide" + uAPIKey, "", true);
            return notificationRES;
        }

        public BasicRES HideFriendrequestMk2(string UserID)
        {
            BasicRES basicRES = Universal.uPUT<BasicRES>(baseUrl + "auth/user/notifications" + UserID + "/hide" + uAPIKey, "", true);
            return basicRES;
        }

        public PlayerModerationsRES Block(string UserID)
        {
            JObject json = new JObject();
            json["blocked"] = UserID;

            PlayerModerationsRES playerModerationsRES = Universal.uPOST<PlayerModerationsRES>(baseUrl + "auth/user/blocks" + uAPIKey, json, true);
            return playerModerationsRES;
        }

        public BasicRES Unblock (string UserID)
        {
            JObject json = new JObject();
            json["blocked"] = UserID;

            BasicRES basicRES = Universal.uPUT<BasicRES>(baseUrl + "auth/user/unblocks" + uAPIKey, json, true);
            return basicRES;
        }
    }
}
