﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Security;
using System.IO;
using System.Web;
using System.Threading;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using static APIdotNet35.Dependencies;
using APIdotNet35.Responses;
using APIdotNet35.Endpoints;
using APIdotNet35.Helpers;
using APIdotNet35;

namespace APIdotNet35.Endpoints
{
    public class Auth
    {
        public Auth(string username, string password)
        {
            Username = username;
            Password = password;
        }

        public AuthUserRES Login()
        {
            AuthUserRES authUserRES = Universal.uGET<AuthUserRES>(baseUrl + "auth/user" + uAPIKey);
            return authUserRES;
        }

        public AuthUserRES UpdateAccountInfo(string userId, string email = null, string birthday = null, string acceptedTOSVersion = null, List<string> tags = null)
        {
            JObject json = new JObject();

            if (email != null)
                json["email"] = email;

            if (birthday != null)
                json["birthday"] = birthday;

            if (acceptedTOSVersion != null)
                json["acceptedTOSVersion"] = acceptedTOSVersion;

            if (tags != null)
                json["tags"] = JToken.FromObject(tags);

            AuthUserRES authUserRES = Universal.uPUT<AuthUserRES>(baseUrl + "users/" + userId + uAPIKey, json, true);
            return authUserRES;
        }
    }
}
