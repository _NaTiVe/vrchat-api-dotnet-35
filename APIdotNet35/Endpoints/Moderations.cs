﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Security;
using System.IO;
using System.Web;
using System.Threading;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using static APIdotNet35.Dependencies;
using APIdotNet35.Responses;
using APIdotNet35.Endpoints;
using APIdotNet35.Helpers;
using APIdotNet35;

namespace APIdotNet35.Endpoints
{
    public class Moderations
    {
        public List<UserModerationsRES> FetchBans()
        {
            List<UserModerationsRES> userModerationsRES = Universal.uGET<List<UserModerationsRES>>(baseUrl + "auth/user/moderations" + uAPIKey);
            return userModerationsRES;
        }

        public List<PlayerModerationsRES> FetchMyModerations()
        {
            List<PlayerModerationsRES> playerModerationsRES = Universal.uGET<List<PlayerModerationsRES>>(baseUrl + "auth/user/playermoderations" + uAPIKey);
            return playerModerationsRES;
        }

        public List<PlayerModerationsRES> FetchModerationsAgainstMe()
        {
            List<PlayerModerationsRES> playerModerationsRES = Universal.uGET<List<PlayerModerationsRES>>(baseUrl + "auth/user/playermoderated" + uAPIKey);
            return playerModerationsRES;
        }

        public PlayerModerationsRES New(string UserID, ModerationType ModerationType)
        {
            JObject json = new JObject();
            if (ModerationType != ModerationType.Unblock)
                json["type"] = ModerationType.ToString().ToLowerInvariant();
            json["moderated"] = UserID;

            PlayerModerationsRES playerModerationsRES = Universal.uPOST<PlayerModerationsRES>(baseUrl + "auth/user/playermoderations" + uAPIKey, json, true);
            return playerModerationsRES;
        }
    }
}
