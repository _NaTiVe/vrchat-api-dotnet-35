﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Security;
using System.IO;
using System.Web;
using System.Threading;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using static APIdotNet35.Dependencies;
using APIdotNet35.Responses;
using APIdotNet35.Endpoints;
using APIdotNet35.Helpers;
using APIdotNet35;

namespace APIdotNet35.Endpoints
{
    public class Favorites
    {
        public FavoritesRES Get(string id)
        {
            FavoritesRES favoritesRES = Universal.uGET<FavoritesRES>(baseUrl + "favorites/" + id + uAPIKey);
            return favoritesRES;
        }

        public List<FavoritesRES> FetchList(TypeOptions? TypeOptions = null)
        {
            string endpoint = "favorites";

            switch (TypeOptions)
            {
                case Responses.TypeOptions.Avatar:
                    endpoint = "avatars/favorites";
                    break;
                case Responses.TypeOptions.Friend:
                    endpoint = "auth/user/friends/favorite";
                    break;
                case Responses.TypeOptions.World:
                    endpoint = "worlds/favorites";
                    break;
            }

            List<FavoritesRES> favoritesRES = Universal.uGET<List<FavoritesRES>>(baseUrl + endpoint + uAPIKey);
            return favoritesRES;
        }

        public FavoritesRES Add(string id, TypeOptions TypeOptions)
        {
            string type = "";
            switch (TypeOptions)
            {
                case Responses.TypeOptions.Avatar:
                    type = "avatar";
                    break;
                case Responses.TypeOptions.Friend:
                    type = "friend";
                    break;
                case Responses.TypeOptions.World:
                    type = "world";
                    break;
            }

            JObject json = new JObject();
            json["type"] = type;
            json["favoriteId"] = id;

            FavoritesRES favoritesRES = Universal.uPOST<FavoritesRES>(baseUrl + "favorites" + uAPIKey, json, true);
            return favoritesRES;
        }
    }
}
