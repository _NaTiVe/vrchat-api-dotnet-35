﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Security;
using System.IO;
using System.Web;
using System.Threading;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using static APIdotNet35.Dependencies;
using APIdotNet35.Responses;
using APIdotNet35.Endpoints;
using APIdotNet35.Helpers;
using APIdotNet35;

namespace APIdotNet35.Endpoints
{
    public class Notifications
    {
        public List<NotificationRES> FetchList()
        {
            List<NotificationRES> notificationRES = Universal.uGETTest<List<NotificationRES>>(baseUrl + "auth/user/notifications" + uAPIKey);
            return notificationRES;
        }

        public NotificationRES Invite(string ReceiverUserID, string SenderName, string WorldID, string InstanceID)
        {
            JObject json = new JObject();
            json["type"] = "invite";
            json["message"] = string.Format("{0} invites you to their world!", SenderName);
            json["details"] = string.Format("{0}:{1}", WorldID, InstanceID);

            NotificationRES notificationRES = Universal.uPOST<NotificationRES>(baseUrl + "user/" + ReceiverUserID + "/notification" + uAPIKey, json, true);
            return notificationRES;
        }

        public NotificationRES Message(string ReceiverUserID, string SenderName, string Message)
        {
            JObject json = new JObject();
            json["type"] = "message";
            json["message"] = string.Format("{0} says: {1}", SenderName, Message);

            NotificationRES notificationRES = Universal.uPOST<NotificationRES>(baseUrl + "user/" + ReceiverUserID + "/notification" + uAPIKey, json, true);
            return notificationRES;
        }

        public NotificationRES VotekickMk1(string ReceiverUserID, string SenderUserID, string SenderName)
        {
            JObject json = new JObject();
            json["type"] = "votetokick";
            json["message"] = string.Format("{0} started a votekick on you.", SenderName);
            json["details"] = string.Format("{0}, {1}", ReceiverUserID, SenderUserID);

            NotificationRES notificationRES = Universal.uPOST<NotificationRES>(baseUrl + "user/" + ReceiverUserID + "/notification" + uAPIKey, json, true);
            return notificationRES;
        }

        public NotificationRES VotekickMk2(string ReceiverUserID, string SenderUserID, string SenderName)
        {
            JObject json = new JObject();
            json["type"] = "votetokick";
            json["message"] = string.Format("{0} started a votekick on you.", SenderName);
            json["details"] = string.Format("{0},{1}", ReceiverUserID, SenderUserID);

            NotificationRES notificationRES = Universal.uPOST<NotificationRES>(baseUrl + "user/" + ReceiverUserID + "/notification" + uAPIKey, json, true);
            return notificationRES;
        }
    }
}
