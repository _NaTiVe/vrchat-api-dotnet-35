﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Security;
using System.IO;
using System.Web;
using System.Threading;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using static APIdotNet35.Dependencies;
using APIdotNet35.Responses;
using APIdotNet35.Endpoints;
using APIdotNet35.Helpers;
using APIdotNet35;

namespace APIdotNet35.Endpoints
{
    public class Friends
    {
        public List<UserRES> FetchList(int? N = null, int? Offset = null, bool? Offline = null)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append(string.Format("&n={0}", N));
            stringBuilder.Append(string.Format("&offset={0}", Offset));

            if (Offline.HasValue && Offline == true)
                stringBuilder.Append(string.Format("&offline={0}", Offline.Value.ToString().ToLowerInvariant()));

            List<UserRES> userRES = Universal.uGET<List<UserRES>>(baseUrl + "auth/user/friends" + uAPIKey + stringBuilder.ToString());
            return userRES;
        }
    }
}
