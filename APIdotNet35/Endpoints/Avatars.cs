﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Security;
using System.IO;
using System.Web;
using System.Threading;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using static APIdotNet35.Dependencies;
using APIdotNet35.Responses;
using APIdotNet35.Endpoints;
using APIdotNet35.Helpers;
using APIdotNet35;

namespace APIdotNet35.Endpoints
{
    public class Avatars
    {
        public AvatarRES Get(string id)
        {
            AvatarRES avatarRES = Universal.uGET<AvatarRES>(baseUrl + "avatars/" + id + uAPIKey);
            return avatarRES;
        }

        public FileRES GetVRCAData(string id)
        {
            AvatarRES avatarRES = Universal.uGET<AvatarRES>(baseUrl + "avatars/" + id + uAPIKey);

            FileRES fileRES = Universal.uGET<FileRES>(avatarRES.assetUrl.Remove(78));
            return fileRES;
        }

        public FileRES GetImageData(string id)
        {
            AvatarRES avatarRES = Universal.uGET<AvatarRES>(baseUrl + "avatars/" + id + uAPIKey);

            FileRES fileRES = Universal.uGET<FileRES>(avatarRES.imageUrl.Remove(78));
            return fileRES;
        }

        public List<AvatarRES> FetchList(string Parameters)
        {
            List<AvatarRES> avatarRES = Universal.uGET<List<AvatarRES>>(baseUrl + "avatars" + uAPIKey + Parameters);
            return avatarRES;
        }

        public List<AvatarRES> FetchList(UserOptionsA? UserOptions = null, bool? Featured = null, string Tag = null,
            string Search = null, int? N = null, int? Offset = null, OrderOptionsA? OrderOptions = null, ReleaseStatusA? ReleaseStatus = null,
            SortOptionsA? SortOptions = null, string MaxUnityVersion = null, string MinUnityVersion = null,
            string MaxAssetVersion = null, string MinAssetVersion = null, string Platform = null)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append(string.Format("&n={0}", N));
            stringBuilder.Append(string.Format("&offset={0}", Offset));

            if (Featured.HasValue)
            {
                stringBuilder.Append(string.Format("&featured={0}", Featured.Value.ToString().ToLowerInvariant()));

                if (Featured.Value && SortOptions.HasValue == false)
                    stringBuilder.Append(string.Format("&sort=order"));
            }
            if (SortOptions.HasValue)
                stringBuilder.Append(string.Format("&sort={0}", SortOptions.Value.ToString().ToLowerInvariant()));
            if (UserOptions.HasValue)
                stringBuilder.Append(string.Format("&user={0}", UserOptions.Value.ToString().ToLowerInvariant()));
            if (OrderOptions.HasValue)
                stringBuilder.Append(string.Format("&order={0}", OrderOptions.Value.ToString().ToLowerInvariant()));
            if (ReleaseStatus.HasValue)
                stringBuilder.Append(string.Format("&releaseStatus={0}", ReleaseStatus.Value.ToString().ToLowerInvariant()));
            if (!string.IsNullOrEmpty(Tag))
                stringBuilder.Append(string.Format("&tag={0}", Tag));
            if (!string.IsNullOrEmpty(Search))
                stringBuilder.Append(string.Format("&search={0}", Search));
            if (!string.IsNullOrEmpty(MaxUnityVersion))
                stringBuilder.Append(string.Format("&maxUnityVersion={0}", MaxUnityVersion));
            if (!string.IsNullOrEmpty(MinUnityVersion))
                stringBuilder.Append(string.Format("&minUnityVersion={0}", MinUnityVersion));
            if (!string.IsNullOrEmpty(MaxAssetVersion))
                stringBuilder.Append(string.Format("&maxAssetVersion={0}", MaxAssetVersion));
            if (!string.IsNullOrEmpty(MinAssetVersion))
                stringBuilder.Append(string.Format("&minAssetVersion={0}", MinAssetVersion));
            if (!string.IsNullOrEmpty(Platform))
                stringBuilder.Append(string.Format("&platform={0}", Platform));

            List<AvatarRES> avatarRES = Universal.uGET<List<AvatarRES>>(baseUrl + "avatars" + uAPIKey + stringBuilder.ToString());
            return avatarRES;
        }
    }
}
