﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Security;
using System.IO;
using System.Web;
using System.Reflection;
using System.Threading;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using static APIdotNet35.Dependencies;
using APIdotNet35.Responses;
using APIdotNet35.Endpoints;
using APIdotNet35.Helpers;
using APIdotNet35;

namespace APIdotNet35.Helpers
{
    public static class Extensions
    {
        public static void GetStatus(this HttpWebResponse response)
        {
            string endpoint = "";
            for (int i = 3; i < response.ResponseUri.Segments.Length; i++)
                endpoint += response.ResponseUri.Segments[i];

            Console.WriteLine("[@" + endpoint + "] Status: " + response.StatusCode + " --- " + response.StatusDescription);
            Console.WriteLine();
        }

        public static void AddAuthHeader(this WebRequest request)
        {
            string base64Auth = Convert.ToBase64String(Encoding.UTF8.GetBytes(Username + ":" + Password));
            request.Headers.Add("Authorization", "Basic " + base64Auth);
            request.PreAuthenticate = true;
        }

        public static void CheckForBans(this HttpWebResponse response, string json)
        {
            if (response.StatusCode == HttpStatusCode.Forbidden)
            {
                if (json.Contains("Temporary ban") || json.ToLower().Contains("temporary ban"))
                {
                    AuthBanRES authBanRES = JsonConvert.DeserializeObject<AuthBanRES>(json);
                    Console.WriteLine("An Error occurred! Banned?");
                    Console.WriteLine("Message: " + authBanRES.error["message"]);
                    Console.WriteLine("Status_Code: " + authBanRES.error["status_code"]);
                    Console.WriteLine("Target: " + authBanRES.target);
                    Console.WriteLine("Reason: " + authBanRES.reason);
                    Console.WriteLine("Expires: " + authBanRES.expires);
                    Console.WriteLine();
                }
                else
                {
                    BasicRES basicRES = JsonConvert.DeserializeObject<BasicRES>(json);
                    Console.WriteLine("An Error occurred!");
                    Console.WriteLine("Message: " + basicRES.error["message"]);
                    Console.WriteLine("Status_Code: " + basicRES.error["status_code"]);
                    Console.WriteLine();
                }
            }
            else
            {
                BasicRES basicRES = JsonConvert.DeserializeObject<BasicRES>(json);
                Console.WriteLine("An Error occurred!");
                Console.WriteLine("Message: " + basicRES.error["message"]);
                Console.WriteLine("Status_Code: " + basicRES.error["status_code"]);
                Console.WriteLine();
            }
        }

        public static void AddProxy(this HttpWebRequest request, string proxyUrl, int port)
        {
            WebProxy proxy = new WebProxy(proxyUrl, port);
            proxy.BypassProxyOnLocal = false;
            request.Proxy = proxy;
        }

        public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
        {
            foreach (var item in source)
                action(item);
        }

        public static void ForEach<T>(this ICollection<T> source, Action<T> action)
        {
            foreach (var item in source)
                action(item);
        }

        public static void ForEach<T>(this List<T> source, Action<T> action)
        {
            foreach (var item in source)
                action(item);
        }

        public static void ForEach<T>(this IList<T> source, Action<T> action)
        {
            foreach (var item in source)
                action(item);
        }
    }
}