﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.IO;
using System.Web;
using System.Threading;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using static APIdotNet35.Dependencies;
using APIdotNet35.Responses;
using APIdotNet35.Endpoints;
using APIdotNet35.Helpers;
using APIdotNet35;

namespace APIdotNet35.Helpers
{
    public class Universal
    {
        public static T uGET<T>(string url)
        {
            ServicePointManager.ServerCertificateValidationCallback = ((object a, X509Certificate b, X509Chain c, SslPolicyErrors d) => true);

            Uri uri = new Uri(url);
            var request = (HttpWebRequest)WebRequest.Create(uri);
            request.Method = "GET";
            AddAuthHeader(request);
            //request.AddAuthHeader();

            string json = "";
            try
            {
                var response = (HttpWebResponse)request.GetResponse();

                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    json = reader.ReadToEnd();
                }

                response.Close();
                response.GetStatus();
                //GetStatus(response);
            }
            catch (WebException x)
            {
                var response = (HttpWebResponse)x.Response;

                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    json = reader.ReadToEnd();
                }

                response.Close();
                response.CheckForBans(json);
                //CheckForBan(response, json);
            }

            T result = JsonConvert.DeserializeObject<T>(json);
            return result;
        }

        public static T uGETP<T>(string url, string proxyUrl, int port)
        {
            ServicePointManager.ServerCertificateValidationCallback = ((object a, X509Certificate b, X509Chain c, SslPolicyErrors d) => true);

            Uri uri = new Uri(url);
            var request = (HttpWebRequest)WebRequest.Create(uri);
            request.AddProxy(proxyUrl, port);
            request.Method = "GET";
            AddAuthHeader(request);
            //request.AddAuthHeader();

            string json = "";
            try
            {
                var response = (HttpWebResponse)request.GetResponse();

                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    json = reader.ReadToEnd();
                }

                response.Close();
                response.GetStatus();
                //GetStatus(response);
            }
            catch (WebException x)
            {
                var response = (HttpWebResponse)x.Response;

                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    json = reader.ReadToEnd();
                }

                response.Close();
                response.CheckForBans(json);
                //CheckForBan(response, json);
            }

            T result = JsonConvert.DeserializeObject<T>(json);
            return result;
        }

        public static T uGETTest<T>(string url)
        {
            ServicePointManager.ServerCertificateValidationCallback = ((object a, X509Certificate b, X509Chain c, SslPolicyErrors d) => true);

            Uri uri = new Uri(url);
            var request = (HttpWebRequest)WebRequest.Create(uri);
            request.Method = "GET";
            AddAuthHeader(request);
            //request.AddAuthHeader();

            string json = "";
            try
            {
                var response = (HttpWebResponse)request.GetResponse();

                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    json = reader.ReadToEnd();
                }

                response.Close();
                response.GetStatus();
                //GetStatus(response);
            }
            catch (WebException x)
            {
                var response = (HttpWebResponse)x.Response;

                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    json = reader.ReadToEnd();
                }

                response.Close();
                response.CheckForBans(json);
                //CheckForBan(response, json);
            }

            Console.WriteLine(json);

            T result = JsonConvert.DeserializeObject<T>(json);
            return result;
        }

        public static T uGETTestP<T>(string url, string proxyUrl, int port)
        {
            ServicePointManager.ServerCertificateValidationCallback = ((object a, X509Certificate b, X509Chain c, SslPolicyErrors d) => true);

            Uri uri = new Uri(url);
            var request = (HttpWebRequest)WebRequest.Create(uri);
            request.AddProxy(proxyUrl, port);
            request.Method = "GET";
            AddAuthHeader(request);
            //request.AddAuthHeader();

            string json = "";
            try
            {
                var response = (HttpWebResponse)request.GetResponse();

                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    json = reader.ReadToEnd();
                }

                response.Close();
                response.GetStatus();
                //GetStatus(response);
            }
            catch (WebException x)
            {
                var response = (HttpWebResponse)x.Response;

                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    json = reader.ReadToEnd();
                }

                response.Close();
                response.CheckForBans(json);
                //CheckForBan(response, json);
            }

            Console.WriteLine(json);

            T result = JsonConvert.DeserializeObject<T>(json);
            return result;
        }

        public static T uDELETE<T>(string url)
        {
            ServicePointManager.ServerCertificateValidationCallback = ((object a, X509Certificate b, X509Chain c, SslPolicyErrors d) => true);

            Uri uri = new Uri(url);
            var request = (HttpWebRequest)WebRequest.Create(uri);
            request.Method = "DELETE";
            AddAuthHeader(request);
            //request.AddAuthHeader();

            string json = "";
            try
            {
                var response = (HttpWebResponse)request.GetResponse();

                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    json = reader.ReadToEnd();
                }

                response.Close();
                response.GetStatus();
                //GetStatus(response);
            }
            catch (WebException x)
            {
                Console.WriteLine(x);

                var response = (HttpWebResponse)x.Response;

                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    json = reader.ReadToEnd();
                }

                response.Close();
                response.CheckForBans(json);
                //CheckForBan(response, json);
            }

            T result = JsonConvert.DeserializeObject<T>(json);
            return result;
        }

        public static T uDELETEP<T>(string url, string proxyUrl, int port)
        {
            ServicePointManager.ServerCertificateValidationCallback = ((object a, X509Certificate b, X509Chain c, SslPolicyErrors d) => true);

            Uri uri = new Uri(url);
            var request = (HttpWebRequest)WebRequest.Create(uri);
            request.AddProxy(proxyUrl, port);
            request.Method = "DELETE";
            AddAuthHeader(request);
            //request.AddAuthHeader();

            string json = "";
            try
            {
                var response = (HttpWebResponse)request.GetResponse();

                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    json = reader.ReadToEnd();
                }

                response.Close();
                response.GetStatus();
                //GetStatus(response);
            }
            catch (WebException x)
            {
                Console.WriteLine(x);

                var response = (HttpWebResponse)x.Response;

                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    json = reader.ReadToEnd();
                }

                response.Close();
                response.CheckForBans(json);
                //CheckForBan(response, json);
            }

            T result = JsonConvert.DeserializeObject<T>(json);
            return result;
        }

        public static T uPOST<T>(string url, string data, bool UTF8Encoding)
        {
            ServicePointManager.ServerCertificateValidationCallback = ((object a, X509Certificate b, X509Chain c, SslPolicyErrors d) => true);

            Uri uri = new Uri(url);
            var request = (HttpWebRequest)WebRequest.Create(uri);
            request.ContentType = "application/json";
            request.Method = "POST";
            AddAuthHeader(request);
            //request.AddAuthHeader();

            if (UTF8Encoding)
                using (StreamWriter writer = new StreamWriter(request.GetRequestStream(), Encoding.UTF8))
                {
                    //var bytes = Encoding.UTF8.GetBytes(data);
                    //writer.Write(bytes);
                    writer.Write(data);
                }
            else
                using (StreamWriter writer = new StreamWriter(request.GetRequestStream()))
                {
                    //var bytes = Encoding.UTF8.GetBytes(data);
                    //writer.Write(bytes);
                    writer.Write(data);
                }

            string json = "";
            try
            {
                var response = (HttpWebResponse)request.GetResponse();

                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    json = reader.ReadToEnd();
                }

                response.Close();
                response.GetStatus();
                //GetStatus(response);
            }
            catch (WebException x)
            {
                Console.WriteLine(x);

                var response = (HttpWebResponse)x.Response;

                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    json = reader.ReadToEnd();
                }

                response.Close();
                response.CheckForBans(json);
                //CheckForBan(response, json);
            }

            T result = JsonConvert.DeserializeObject<T>(json);
            return result;
        }

        public static T uPOST<T>(string url, JObject data, bool UTF8Encoding)
        {
            ServicePointManager.ServerCertificateValidationCallback = ((object a, X509Certificate b, X509Chain c, SslPolicyErrors d) => true);

            Uri uri = new Uri(url);
            var request = (HttpWebRequest)WebRequest.Create(uri);
            request.ContentType = "application/json";
            request.Method = "POST";
            AddAuthHeader(request);
            //request.AddAuthHeader();

            if (UTF8Encoding)
                using (StreamWriter writer = new StreamWriter(request.GetRequestStream(), Encoding.UTF8))
                {
                    //var bytes = Encoding.UTF8.GetBytes(data);
                    //writer.Write(bytes);
                    writer.Write(data.ToString());
                }
            else
                using (StreamWriter writer = new StreamWriter(request.GetRequestStream()))
                {
                    //var bytes = Encoding.UTF8.GetBytes(data);
                    //writer.Write(bytes);
                    writer.Write(data.ToString());
                }

            string json = "";
            try
            {
                var response = (HttpWebResponse)request.GetResponse();

                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    json = reader.ReadToEnd();
                }

                response.Close();
                response.GetStatus();
                //GetStatus(response);
            }
            catch (WebException x)
            {
                Console.WriteLine(x);

                var response = (HttpWebResponse)x.Response;

                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    json = reader.ReadToEnd();
                }

                response.Close();
                response.CheckForBans(json);
                //CheckForBan(response, json);
            }

            T result = JsonConvert.DeserializeObject<T>(json);
            return result;
        }

        public static T uPOSTP<T>(string url, JObject data, bool UTF8Encoding, string proxyUrl, int port)
        {
            ServicePointManager.ServerCertificateValidationCallback = ((object a, X509Certificate b, X509Chain c, SslPolicyErrors d) => true);

            Uri uri = new Uri(url);
            var request = (HttpWebRequest)WebRequest.Create(uri);
            request.ContentType = "application/json";
            request.AddProxy(proxyUrl, port);
            request.Method = "POST";
            AddAuthHeader(request);
            //request.AddAuthHeader();

            if (UTF8Encoding)
                using (StreamWriter writer = new StreamWriter(request.GetRequestStream(), Encoding.UTF8))
                {
                    //var bytes = Encoding.UTF8.GetBytes(data);
                    //writer.Write(bytes);
                    writer.Write(data.ToString());
                }
            else
                using (StreamWriter writer = new StreamWriter(request.GetRequestStream()))
                {
                    //var bytes = Encoding.UTF8.GetBytes(data);
                    //writer.Write(bytes);
                    writer.Write(data.ToString());
                }

            string json = "";
            try
            {
                var response = (HttpWebResponse)request.GetResponse();

                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    json = reader.ReadToEnd();
                }

                response.Close();
                response.GetStatus();
                //GetStatus(response);
            }
            catch (WebException x)
            {
                Console.WriteLine(x);

                var response = (HttpWebResponse)x.Response;

                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    json = reader.ReadToEnd();
                }

                response.Close();
                response.CheckForBans(json);
                //CheckForBan(response, json);
            }

            T result = JsonConvert.DeserializeObject<T>(json);
            return result;
        }

        public static T uPUT<T>(string url, string data, bool UTF8Encoding)
        {
            ServicePointManager.ServerCertificateValidationCallback = ((object a, X509Certificate b, X509Chain c, SslPolicyErrors d) => true);

            Uri uri = new Uri(url);
            var request = (HttpWebRequest)WebRequest.Create(uri);
            request.ContentType = "application/json";
            request.Method = "PUT";
            AddAuthHeader(request);
            //request.AddAuthHeader();

            if (UTF8Encoding)
                using (StreamWriter writer = new StreamWriter(request.GetRequestStream(), Encoding.UTF8))
                {
                    //var bytes = Encoding.UTF8.GetBytes(data);
                    //writer.Write(bytes);
                    writer.Write(data);
                }
            else
                using (StreamWriter writer = new StreamWriter(request.GetRequestStream()))
                {
                    //var bytes = Encoding.UTF8.GetBytes(data);
                    //writer.Write(bytes);
                    writer.Write(data);
                }

            string json = "";
            try
            {
                var response = (HttpWebResponse)request.GetResponse();

                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    json = reader.ReadToEnd();
                }

                response.Close();
                response.GetStatus();
                //GetStatus(response);
            }
            catch (WebException x)
            {
                Console.WriteLine(x);

                var response = (HttpWebResponse)x.Response;

                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    json = reader.ReadToEnd();
                }

                response.Close();
                response.CheckForBans(json);
                //CheckForBan(response, json);
            }

            T result = JsonConvert.DeserializeObject<T>(json);
            return result;
        }

        public static T uPUT<T>(string url, JObject data, bool UTF8Encoding)
        {
            ServicePointManager.ServerCertificateValidationCallback = ((object a, X509Certificate b, X509Chain c, SslPolicyErrors d) => true);

            Uri uri = new Uri(url);
            var request = (HttpWebRequest)WebRequest.Create(uri);
            request.ContentType = "application/json";
            request.Method = "PUT";
            AddAuthHeader(request);
            //request.AddAuthHeader();
            if (UTF8Encoding)
                using (StreamWriter writer = new StreamWriter(request.GetRequestStream(), Encoding.UTF8))
                {
                    //var bytes = Encoding.UTF8.GetBytes(data);
                    //writer.Write(bytes);
                    writer.Write(data.ToString());
                }
            else
                using (StreamWriter writer = new StreamWriter(request.GetRequestStream()))
                {
                    //var bytes = Encoding.UTF8.GetBytes(data);
                    //writer.Write(bytes);
                    writer.Write(data.ToString());
                }

            string json = "";
            try
            {
                var response = (HttpWebResponse)request.GetResponse();

                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    json = reader.ReadToEnd();
                }

                response.Close();
                response.GetStatus();
                //GetStatus(response);
            }
            catch (WebException x)
            {
                Console.WriteLine(x);

                var response = (HttpWebResponse)x.Response;

                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    json = reader.ReadToEnd();
                }

                response.Close();
                response.CheckForBans(json);
                //CheckForBan(response, json);
            }

            T result = JsonConvert.DeserializeObject<T>(json);
            return result;
        }

        public static T uPUTP<T>(string url, JObject data, bool UTF8Encoding, string proxyUrl, int port)
        {
            ServicePointManager.ServerCertificateValidationCallback = ((object a, X509Certificate b, X509Chain c, SslPolicyErrors d) => true);

            Uri uri = new Uri(url);
            var request = (HttpWebRequest)WebRequest.Create(uri);
            request.ContentType = "application/json";
            request.AddProxy(proxyUrl, port);
            request.Method = "PUT";
            AddAuthHeader(request);
            //request.AddAuthHeader();
            if (UTF8Encoding)
                using (StreamWriter writer = new StreamWriter(request.GetRequestStream(), Encoding.UTF8))
                {
                    //var bytes = Encoding.UTF8.GetBytes(data);
                    //writer.Write(bytes);
                    writer.Write(data.ToString());
                }
            else
                using (StreamWriter writer = new StreamWriter(request.GetRequestStream()))
                {
                    //var bytes = Encoding.UTF8.GetBytes(data);
                    //writer.Write(bytes);
                    writer.Write(data.ToString());
                }

            string json = "";
            try
            {
                var response = (HttpWebResponse)request.GetResponse();

                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    json = reader.ReadToEnd();
                }

                response.Close();
                response.GetStatus();
                //GetStatus(response);
            }
            catch (WebException x)
            {
                Console.WriteLine(x);

                var response = (HttpWebResponse)x.Response;

                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    json = reader.ReadToEnd();
                }

                response.Close();
                response.CheckForBans(json);
                //CheckForBan(response, json);
            }

            T result = JsonConvert.DeserializeObject<T>(json);
            return result;
        }

        public static void AddAuthHeader(WebRequest request)
        {
            string base64Auth = Convert.ToBase64String(Encoding.UTF8.GetBytes(Username + ":" + Password));
            request.Headers.Add("Authorization", "Basic " + base64Auth);
            request.PreAuthenticate = true;
        }

        public static void CheckForBan(HttpWebResponse response, string json)
        {
            if (response.StatusCode == HttpStatusCode.Forbidden)
            {
                if (json.Contains("Temporary ban") || json.ToLower().Contains("temporary ban"))
                {
                    AuthBanRES authBanRES = JsonConvert.DeserializeObject<AuthBanRES>(json);
                    Console.WriteLine("An Error occurred! Banned?");
                    Console.WriteLine("Message: " + authBanRES.error["message"]);
                    Console.WriteLine("Status_Code: " + authBanRES.error["status_code"]);
                    Console.WriteLine("Target: " + authBanRES.target);
                    Console.WriteLine("Reason: " + authBanRES.reason);
                    Console.WriteLine("Expires: " + authBanRES.expires);
                    Console.WriteLine();
                }
                else
                {
                    BasicRES basicRES = JsonConvert.DeserializeObject<BasicRES>(json);
                    Console.WriteLine("An Error occurred!");
                    Console.WriteLine("Message: " + basicRES.error["message"]);
                    Console.WriteLine("Status_Code: " + basicRES.error["status_code"]);
                    Console.WriteLine();
                }
            }
            else
            {
                BasicRES basicRES = JsonConvert.DeserializeObject<BasicRES>(json);
                Console.WriteLine("An Error occurred!");
                Console.WriteLine("Message: " + basicRES.error["message"]);
                Console.WriteLine("Status_Code: " + basicRES.error["status_code"]);
                Console.WriteLine();
            }
        }

        public static void GetStatus(HttpWebResponse response)
        {
            string responseUri = response.ResponseUri.AbsoluteUri;
            responseUri.Remove(0, 29);
            responseUri.Replace(uAPIKey, "");

            Console.WriteLine("[@" + responseUri + "] Status: " + response.StatusCode + " --- " + response.StatusDescription);
            Console.WriteLine();
        }

        public static void AddProxy(HttpWebRequest request, string proxyUrl, int port)
        {
            WebProxy proxy = new WebProxy(proxyUrl, port);
            proxy.BypassProxyOnLocal = false;
            request.Proxy = proxy;
        }
    }
}