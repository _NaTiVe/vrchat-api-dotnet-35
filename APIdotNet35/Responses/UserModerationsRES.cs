﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Security;
using System.IO;
using System.Web;
using System.Threading;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using static APIdotNet35.Dependencies;
using APIdotNet35.Responses;
using APIdotNet35.Endpoints;
using APIdotNet35.Helpers;
using APIdotNet35;

namespace APIdotNet35.Responses
{
    public class UserModerationsRES
    {
        public string id { get; set; }
        public string type { get; set; }
        public string targetUserId { get; set; }
        public string targetDisplayName { get; set; }
        public string reason { get; set; }
        public string created { get; set; }
        public string expires { get; set; }
        public string worldId { get; set; }
        public string instanceId { get; set; }
        public string ip { get; set; }
        public string mac { get; set; }
        public bool active { get; set; }
    }
}
