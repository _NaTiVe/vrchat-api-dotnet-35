﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Security;
using System.IO;
using System.Web;
using System.Threading;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Converters;

using static APIdotNet35.Dependencies;
using APIdotNet35.Responses;
using APIdotNet35.Endpoints;
using APIdotNet35.Helpers;
using APIdotNet35;

namespace APIdotNet35.Responses
{
    public class WorldsRES
    {
        public string id { get; set; }
        public string name { get; set; }
        public string authorName { get; set; }
        public string capacity { get; set; }
        public int totalLikes { get; set; }
        public int totalVisits { get; set; }
        public string imageUrl { get; set; }
        public string thumbnailImageUrl { get; set; }
        public bool isSecure { get; set; } // Unknown
        [JsonConverter(typeof(StringEnumConverter))]
        public ReleaseStatus releaseStatus { get; set; }
        public string organization { get; set; } // Unknown
        public int occupants { get; set; }
    }
}
