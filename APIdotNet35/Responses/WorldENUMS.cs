﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Security;
using System.IO;
using System.Web;
using System.Threading;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Converters;

using static APIdotNet35.Dependencies;
using APIdotNet35.Responses;
using APIdotNet35.Endpoints;
using APIdotNet35.Helpers;
using APIdotNet35;

namespace APIdotNet35.Responses
{
    public enum UserOptions
    {
        Me,
        Friends,
    }

    public enum SortOptions
    {
        Popularity,
        Created,
        Updated,
        Order,
    }

    public enum ReleaseStatus
    {
        Public,
        Private,
        All,
        Hidden,
    }

    public enum WorldGroups
    {
        Any,
        Active,
        Recent,
        Favorite,
    }
}
