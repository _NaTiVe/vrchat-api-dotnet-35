﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Security;
using System.IO;
using System.Web;
using System.Threading;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using static APIdotNet35.Dependencies;
using APIdotNet35.Responses;
using APIdotNet35.Endpoints;
using APIdotNet35.Helpers;
using APIdotNet35;

namespace APIdotNet35.Responses
{
    public class Versions
    {
        public int version { get; set; }
        public string status { get; set; }
        [JsonProperty(PropertyName = "created_at")]
        public string createdTime { get; set; }
        public File file { get; set; }
        public Delta delta { get; set; }
        public Signature signature { get; set; }
        public bool deleted { get; set; }
    }

    public class File
    {
        public string fileName { get; set; }
        public string url { get; set; }
        public string md5 { get; set; }
        public int sizeInBytes { get; set; }
        public string status { get; set; }
        public string category { get; set; }
        public string uploadId { get; set; }
    }

    public class Delta
    {
        public string fileName { get; set; }
        public string url { get; set; }
        public string md5 { get; set; }
        public int sizeInBytes { get; set; }
        public string status { get; set; }
        public string category { get; set; }
        public string uploadId { get; set; }
    }

    public class Signature
    {
        public string fileName { get; set; }
        public string url { get; set; }
        public string md5 { get; set; }
        public int sizeInBytes { get; set; }
        public string status { get; set; }
        public string category { get; set; }
        public string uploadId { get; set; }
    }

    public class FileRES
    {
        public string id { get; set; }
        public string name { get; set; }
        public string ownerId { get; set; }
        public string mimeType { get; set; }
        public string extension { get; set; }
        public List<Versions> versions { get; set; }
    }
}
