﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Security;
using System.IO;
using System.Web;
using System.Threading;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Converters;

using static APIdotNet35.Dependencies;
using APIdotNet35.Responses;
using APIdotNet35.Endpoints;
using APIdotNet35.Helpers;
using APIdotNet35;

namespace APIdotNet35.Responses
{
    public class UnityPackage
    {
        public string id { get; set; }
        public string assetUrl { get; set; }
        public string pluginUrl { get; set; }
        public string unityVersion { get; set; }
        public int unitySortNumber { get; set; }
        public int assetVersion { get; set; }
        public string platform { get; set; }
        [JsonProperty(PropertyName = "created_at")]
        public string createdTime { get; set; }
    }

    public class WorldInstance
    {
        public string id { get; set; }
        public int occupants { get; set; }
    }

    public class WorldRES
    {
        public string id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public bool featured { get; set; }
        public string authorId { get; set; }
        public string authorName { get; set; }
        public int totalLikes { get; set; }
        public int totalVisits { get; set; }
        public short capacity { get; set; }
        public List<string> tags { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public ReleaseStatus releaseStatus { get; set; }
        public string imageUrl { get; set; }
        public string thumbnailImageUrl { get; set; }
        public string assetUrl { get; set; }
        public JObject assetUrlObject { get; set; }
        public string pluginUrl { get; set; }
        public JObject pluginUrlObject { get; set; }
        public string unityPackageUrl { get; set; }
        public JObject unityPackageUrlObject { get; set; }
        [JsonProperty(PropertyName = "namespace")]
        public string nameSpace { get; set; } // Unknown
        public bool unityPackageUpdated { get; set; } // Unknown
        public List<UnityPackage> unityPackages { get; set; }
        public bool isSecure { get; set; } // Unknown
        public bool isLockdown { get; set; } // Unknown
        public int version { get; set; }
        public string organization { get; set; } // Unknown
        public List<WorldInstance> instances { get; set; }
        public int occupants { get; set; }
        public int privateOccupants { get; set; }
        public int publicOccupants { get; set; }
    }
}
