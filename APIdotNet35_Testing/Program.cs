﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Security;
using System.IO;
using System.Web;
using System.Threading;
using System.Reflection;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using static APIdotNet35.Dependencies;
using APIdotNet35.Responses;
using APIdotNet35.Endpoints;
using APIdotNet35.Helpers;
using APIdotNet35;

namespace APIdotNet35_Testing
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Username");
            string username = Console.ReadLine();
            Console.WriteLine();

            Console.WriteLine("Password");
            string password = Console.ReadLine();
            Console.WriteLine();

            Client client = new Client(username, password);

            AuthUserRES authUserRES = client.Auth.Login();

            (authUserRES.GetType().GetProperties().ToList()).ForEach(x =>
            {
                if (x.Name == "pastDisplayNames")
                {
                    Console.WriteLine("--- " + x.Name + " start " + ((List<PastDisplayName>)x.GetValue(authUserRES, null)).Count + " ---");
                    ((List<PastDisplayName>)x.GetValue(authUserRES, null)).ForEach(i =>
                    {
                        Console.WriteLine(i.displayName);
                        Console.WriteLine(i.updated_at);
                    });
                    Console.WriteLine("--- " + x.Name + " end ---");
                }
                else if (x.Name == "friends")
                {
                    Console.WriteLine("--- " + x.Name + " start " + ((List<string>)x.GetValue(authUserRES, null)).Count + " ---");
                    ((List<string>)x.GetValue(authUserRES, null)).ForEach(i =>
                    {
                        Console.WriteLine(i);
                    });
                    Console.WriteLine("--- " + x.Name + " end ---");
                }
                else if (x.Name == "events")
                {
                    Console.WriteLine("--- " + x.Name + " start " + ((List<string>)x.GetValue(authUserRES, null)).Count + " ---");
                    ((List<string>)x.GetValue(authUserRES, null)).ForEach(i =>
                    {
                        Console.WriteLine(i);
                    });
                    Console.WriteLine("--- " + x.Name + " end ---");
                }
                else if (x.Name == "tags")
                {
                    Console.WriteLine("--- " + x.Name + " start " + ((List<string>)x.GetValue(authUserRES, null)).Count + " ---");
                    ((List<string>)x.GetValue(authUserRES, null)).ForEach(i =>
                    {
                        Console.WriteLine(i);
                    });
                    Console.WriteLine("--- " + x.Name + " end ---");
                }
                else
                    Console.WriteLine(x.Name + " -:- " + x.GetValue(authUserRES, null));
            });

            List<NotificationRES> notificationRES = client.Notifications.FetchList();

            notificationRES.ForEach(x =>
            {
                x.GetType().GetProperties().ForEach(i =>
                {
                    Console.WriteLine(i.Name + " -:- " + i.GetValue(x, null));
                });
            });

            Console.ReadKey();
        }
    }
}
